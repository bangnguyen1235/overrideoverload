﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverrideOverloading
{
    internal class Rectangle : Shape
    {  
   
        public override int Area(int width, int height)
        {
            return base.Area(width, height);
        }
        public void Same(int sameSize) { 
            Width = sameSize;   
            Height = sameSize;
            Console.WriteLine(Width + " " + Height);
        }

        public void Same(int width, int height) {
            Width = width;
            Height = height;
            Console.WriteLine(Width + " " + Height);
        }

        public static Rectangle operator +(Rectangle a, Rectangle b) {
            Rectangle minus = new Rectangle() { Width = a.Width + b.Width, Height = a.Height + b.Height };
            ShowDetail(minus);
            return minus;
        }
        public static Rectangle operator -(Rectangle a, Rectangle b)
        {
            Rectangle plus = new Rectangle(){ Width = a.Width - b.Width, Height = a.Height - b.Height };
            ShowDetail(plus);
            return plus;
        }
        public static void ShowDetail(Rectangle rectange) {
            Console.WriteLine("Height: " +rectange.Height + " " + "Width: " + rectange.Width);
        }    
    }
}
