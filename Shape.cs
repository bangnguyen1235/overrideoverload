﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverrideOverloading
{
    internal class Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public virtual int Area(int width, int height) { 
            return width * height;
        }
    }
}
