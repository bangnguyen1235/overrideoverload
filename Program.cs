﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverrideOverloading
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.Width = 12;
            rectangle.Height = 9;

            //rectangle.Same(10, 10);
            //rectangle.Same(20);
            

            Rectangle rectangle2 = new Rectangle();
            rectangle.Width = 20;
            rectangle.Height = 19;

            Rectangle rectangle3 = rectangle + rectangle2;
            Rectangle rectangle4 = rectangle2 - rectangle;

   

            Console.ReadKey();

            
        }
    }
}
